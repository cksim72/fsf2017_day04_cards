var express = require("express");
var path = require("path");

// Create an instance of express
var app=express();

// Deck of Cards
var deckOfCards = function(shuf) {
    var suit = ["Spade","Diamond","Club","Heart"];
    var name = ["A","2","3","4","5","6","7","8","9","10","J","Q","K"];
    var value = [1,2,3,4,5,6,7,8,9,10,11,12,13];

    var theDeck = [];

    for (var s in suit) {
        for (var n in name)
            theDeck.push({
                suit: suit[s],
                name: name[n],
                value: value[n]
            });
    }

    // Shuffle
    shuf = shuf || 3;

    for (var times=0; times < shuf; times++)
        for (var i=0; i < theDeck.length; i++) {
            var dst = parseInt((Math.random() * theDeck.length));
            var swapVal = theDeck[i];
            theDeck[i] = theDeck[dst];
            theDeck[dst] = swapVal;
        }

    return (theDeck);
}

// Define routes
// /cards/
app.get("/cards", function(req, resp){
    var empNo = req.params.empNo;

    resp.status(200);
    resp.type("application/json");

    resp.json(deckOfCards());
        
});

// /card/?
app.get("/card/:num", function(req, resp){
    var num = req.params.num;

    if ((num > 0) && (num < 53)) {
        var cardStack=[];

        cardStack=deckOfCards().slice(0,num);
 //       console.log(cardStack);

        resp.type("application/json")
            .status(200)
            .json(cardStack);

    }
    else
    {    
        resp.type("text/html").status(200).send("<h1> Invalid card number. Number should be between 1 and 52.</h1>")
    }
   
});

app.use(function(req,resp) {
    resp.type("text/html");
    resp.status(200);
    resp.send('<h1> Please enter in the format "/cards/" or "/card/N/", where N is a number between 1 and 52</h1>');
});

// Configure the port 
app.set("port", process.env.APP_PORT || 3000);

// Start the server
app.listen(app.get("port"), function() {
    console.log("Application started at %s on port %d", new Date(), app.get("port"));
});
